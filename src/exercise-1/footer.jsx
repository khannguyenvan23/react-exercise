import React, { Component } from 'react';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import promotion_1 from './img/promotion_1.png'
import promotion_2 from './img/promotion_2.png'
import promotion_3 from './img/promotion_3.jpg'
class Footer extends Component {
    render() {
        return (
            <div className="btn-dark" id="promotion">
                <h2 className="text-center">PROMOTION</h2>
                <Container style={
                    {   width: "90%",
                        backgroundColor: "white",
                        color: "white"
                    }
                }>
                    <Row>
                        <Col md={4} className="py-2">
                            <img src={promotion_1} alt="promotion_1" className="w-100"/>
                        </Col>
                        <Col md={4}
                        className="py-2">
                            <img src={promotion_2} alt="promotion_2" className="w-100"/>
                        </Col>
                        <Col md={4}className="py-2">
                            <img src={promotion_3} alt="promotion_3" className="w-100"/>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Footer;