import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

class Header extends Component {
    render() {
        return (
            <>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="#home">Cybersoft</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                    <Nav>
                    <Nav.Link href="#tryGlassess">Thử kính</Nav.Link>
                    <Nav.Link href="#carousel">Carousel</Nav.Link>
                    <Nav.Link href="#products">Products</Nav.Link>
                    <Nav.Link href="#promotion">Promotion</Nav.Link>
                    </Nav>

                </Navbar.Collapse>
                </Navbar>
                </>
        )
    }
}

export default Header;