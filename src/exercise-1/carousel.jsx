import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel'
import slice1 from './img/slide_1.jpg'
import slice2 from './img/slide_1.jpg'
import slice3 from './img/slide_1.jpg'
class CarouselComponet extends Component {
    render() {
        return (
            <Carousel id="carousel">
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slice1}
                alt="First slide"
              />

            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slice2}
                alt="Third slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slice3}
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        );
    }
}

export default CarouselComponet;