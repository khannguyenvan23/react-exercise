import React from 'react';
import Col from 'react-bootstrap/Col'

const customStyle = {
    img: {
        maxWidth: "100%",
        maxHeight: "200px"
    },
    card: {
        backgroundColor: "white",
        color: "black",
        height:"99%",
        border: "solid black 1px",
        textAlign: "center"
    }
}
function Product(props) {
    const product = props;
    return (
        <Col sm={6} md={3}>
        <div style={customStyle.card} className="px-auto">
            <img src={product.img} alt={product.name} style={customStyle.img}></img>
            <h4>{product.name}</h4>
            <p>{product.desc}</p>
            <p><button className="btn btn-primary">Details »</button> <button className="btn btn-danger">Cart</button></p>
        </div>
        </Col>
    );

}

export default Product;