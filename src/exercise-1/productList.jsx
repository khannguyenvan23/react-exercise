import React, { Component } from 'react';
import Product from './product.jsx'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'


class ProductList extends Component {
    // { id: "sp_1", name: "iphoneX", price: "30.000.000 VNÄ�", screen: "screen_1", backCamera: "backCamera_1", frontCamera: "frontCamera_1", img: "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg", desc: "iPhone X features a new all-screen design. Face ID, which makes your face your password", }
    render() {
        const DATA = this.props.data
        const mapProduct = DATA.map(
            (item)=>{
                const {id,name, img, desc} = item;
                return(
                <Product key={id.toString()}
                name={name}
                img ={img}
                desc={desc}
                />
                )
            }
        )
        return (
            <div className="bg-dark text-white" id="products">
                <h2 className="text-center">BEST SMARTPHONE</h2>
                <Container>
                    <Row>
                        { mapProduct }
                    </Row>
                </Container>
            </div>

        );
    }
}

export default ProductList;