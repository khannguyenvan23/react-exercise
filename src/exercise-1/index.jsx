import React, { Component  } from 'react';
import { DATA } from './data.js'
import Container from 'react-bootstrap/Container'
import Header from './header.jsx'
import CarouselComponet from './carousel.jsx'
import ProductList from './productList.jsx'
import Footer from './footer.jsx'


// RUN AP
// ==================================
class PhoneShop extends Component {
    render() {
        return (
            <Container fluid style={
                {
                    padding:0
                }
            }>
                <Header />
                <CarouselComponet />
                <ProductList data={DATA}/>
                <Footer />
            </Container>
        );
    }
}

export default PhoneShop;