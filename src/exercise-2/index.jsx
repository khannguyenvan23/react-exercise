import React, { Component } from 'react';
import { DATA } from './data.js'
import { Container, Card, Row, Col, Button, CardDeck } from 'react-bootstrap'
import backgroundImage from './img/background.jpg'
import model from './img/model.jpg'

const cumstomStyle = {
    container:{
        backgroundImage: `url(${backgroundImage})`,
        height: "100vh",
        padding: "0"
    },
    header: {
        width: "100%",
        height: "15vh",
        background: "rgb(0 0 0 / 69%)",
        color: "white",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    main:{
        height: "85vh",
        background: "#00000082",
    },
    center: {
        display: "flex",
        justifyContent: "center",
    },
    CardDeck: {
        width: "40rem",
        justifyContent: "space-around"
    },
    Card: {
        flex: "none",
        width: "196.391px",
    },
    cardImg: {
        width: "100%",
    },
    cardImgOverlay: {
        padding: 0,
    },
    CardHeader:{
        height: "65%",
        width: "100%",
        postition: "relative",
    },
    cardGlassess:{
        position: "absolute",
        width:"100px",
        top:"63px",
        left:"48px",
        // transform:"translate(50%,50%)",
        zIndex:"1"
    },
    CardFooter:{
        height: "35%",
        width: "100%",
        fontSize: "12px",
        padding: "4px",
        background:"#ec990070",
    },
    CardTile:{
        fontSize:"1.1em",
        marginBottom:0,
        padding:0,
        fontWeight:"700",
        textTransform: "uppercase",
        color:"magenta"
    },


}
//Display view
function TryClassessView(props){

    const {url,  name, desc} = props.data;
    return(
        <Container style={cumstomStyle.center}>
        <CardDeck style={cumstomStyle.CardDeck}>
            <Card style={cumstomStyle.Card}>
            <Card.Img src={model} alt="Gai Xinh" style={cumstomStyle.cardImg}/>
            <Card.ImgOverlay style={cumstomStyle.cardImgOverlay}>
            <div style={cumstomStyle.CardHeader}>
                <div style={cumstomStyle.cardGlassess}>
                    <img src={url}  alt="kinh dep" className="w-100" style={
                        {
                            opacity:"0.7"
                        }
                    }/>
                </div>
            </div>
            <div style={cumstomStyle.CardFooter}>
                <Card.Title style={cumstomStyle.CardTile}>
                { name }
                </Card.Title>
                <Card.Text style={cumstomStyle.CardText}>
                { desc }
                </Card.Text>
            </div>
            </Card.ImgOverlay>
            </Card>
            <Card style={cumstomStyle.Card}>
            <Card.Img src={model} alt="Gai Xinh" style={cumstomStyle.cardImg}/>
            </Card>

        </CardDeck>
        </Container>
    )
}
// Display product and get event
function TryClassessProducts(props){
    const id = props.id
    return(
        <Col>
            <Button variant="light outline-secondary" style={
                {
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                }
            } onClick={()=>props.onCLick(id)}
            >
            <img src={props.url} style={
                {
                    width:"70px",
                    height: "30px"
                }
            } alt={"kinh xinh " + id}/>
            </Button>
        </Col>
    )
}
// controller app
class TryClassessCtr extends Component {
    constructor(props){
        super(props)
        this.state = {
            url: './img/v1.png',
            name:'GUCCI G8850U',
            desc:'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ',
        }
    }
    handleClick(id){
        // Find product.id === id
        const product = this.props.data.find(
            (item)=>item.id === id
        );
        // Get url name desc from this object
        const {url,  name, desc} = product;
        // update setState
        this.setState({
            url: url,
            name: name,
            desc: desc,
        })
    }
    render(){
        return(
            <>
            <TryClassessView data={this.state}/>
            <Container className="mt-5 py-2" style={
                {
                    backgroundColor:"white",
                    display: "flex",
                    justifyContent:"center",
                    alignItems:"center",
                    borderRadius:"10px"
                    }
            }>
                <Row style={
                    {
                        height:"70%"
                    }
                }>

                {this.props.data.map(
                    (item)=>{
                        const {id, url} = item;
                        return <TryClassessProducts key={id} id={id}
                            url={url}
                            onCLick={()=>this.handleClick(id)}
                        />;
                    }
                )}
                </Row>
            </Container>
        </>
        )
    }
}
// init APP
class TryGlassesAPP extends Component {
    render() {

        return (
            <Container style={cumstomStyle.container} id="tryGlassess" fluid>
                <div style={cumstomStyle.header}>
                    <h1>TRY CLASSESS APP ONLINE</h1>
                </div>
                <div style={cumstomStyle.main} className="pt-2">
                    <TryClassessCtr data={DATA}/>
                </div>
            </Container>
        );
    }
}

export default TryGlassesAPP;