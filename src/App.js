import React, { Component } from 'react';
import PhoneShop from './exercise-1'
import TryGlassesAPP from './exercise-2'
class App extends Component {
    render() {
        return (
            <>
                <PhoneShop />
                <TryGlassesAPP/>
            </>
        );
    }
}

export default App;