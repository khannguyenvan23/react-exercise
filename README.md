# BAI TAP REACT JS
## Lưu ý: phần trang chủ chứa cả 2 demo smartphone ở trên và thử kính ở dưới
## Cấu trúc

    1. src/exercise-1: smartphone
    2. src/exercise-2: thử kính
    3. app.js: khởi tạo 2 app


1.Dàn layout sau bằng cách chia component

Link ảnh toàn trang: http://lms.myclass.vn/pluginfile.php/21568/mod_assign/intro/117594444_4825894960757623_3858720861113740907_n.png

Link hình dùng trong website: http://lms.myclass.vn/pluginfile.php/21568/mod_assign/intro/img.zip

 ***  Data sản phẩm:  [ { id: "sp_1", name: "iphoneX", price: "30.000.000 VNÄ�", screen: "screen_1", backCamera: "backCamera_1", frontCamera: "frontCamera_1", img: "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg", desc: "iPhone X features a new all-screen design. Face ID, which makes your face your password", }, { id: "sp_2", name: "Note 7", price: "20.000.000 VNÄ�", screen: "screen_2", backCamera: "backCamera_2", frontCamera: "frontCamera_2", img: "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png", desc: "The Galaxy Note7 comes with a perfectly symmetrical design for good reason", }, { id: "sp_3", name: "Vivo", price: "10.000.000 VNÄ�", screen: "screen_3", backCamera: "backCamera_3", frontCamera: "frontCamera_3", img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg", desc: "A young global smartphone brand focusing on introducing perfect sound quality", }, { id: "sp_4", name: "Blacberry", price: "15.000.000 VNÄ�", screen: "screen_4", backCamera: "backCamera_4", frontCamera: "frontCamera_4", img: "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg", desc: "BlackBerry is a line of smartphones, tablets, and services originally designed", }, ]

**Gợi ý: map từ data sản phẩm ra thành component ProductItem, dùng props truyền dữ liệu vào từng component ProductItem để ra đc giao diện khác nhau, còn lại làm tĩnh. Đây là chức năng nâng cao thử, ai làm đc thì tốt, không làm đc thì để bữa sau mình hướng dẫn, lập ra 4 component ProductItem giống nhau là được rồi

VD : data.map((item) => { return <ProductItem prod={item} /> }), rồi ở bên <ProductItem /> lấy prod do thằng cha truyền dô để in ra giao diện

2.Xây dựng app thử kính online

http://lms.myclass.vn/pluginfile.php/21568/mod_assign/intro/img.png

**Link lấy hình ảnh: https://bom.to/cSQNWw

**Data sản phẩm:

let arrProduct = [

    { id: 1, price: 30, name: 'GUCCI G8850U', url: './glassesImage/v1.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 2, price: 50, name: 'GUCCI G8759H', url: './glassesImage/v2.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 3, price: 30, name: 'DIOR D6700HQ', url: './glassesImage/v3.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 4, price: 30, name: 'DIOR D6005U', url:  './glassesImage/v4.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 5, price: 30, name: 'PRADA P8750', url: './glassesImage/v5.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 6, price: 30, name: 'PRADA P9700', url: './glassesImage/v6.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 7, price: 30, name: 'FENDI F8750', url: './glassesImage/v7.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 8, price: 30, name: 'FENDI F8500', url: './glassesImage/v8.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 9, price: 30, name: 'FENDI F4300', url: './glassesImage/v9.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

   ];

**Lưu ý:

 - Trên lớp mình hướng dẫn sử dụng hình bằng cách import hình vào component để dùng, thưc tế sẽ dùng như v

  - tuy nhiên đây là bài tập, có nhiều hình, nên để làm các bạn paste thư mục hình ảnh vào folder public, rồi dùng link bình thường, không cần phải import

  - Làm giống như bài đổi màu xe, tạo một state lưu lại đối tượng kính được chọn là được

  - Chỉ cần 1 component, không cần tách nhiều